<?php
/**
 * Plugin Name: WPMVC
 * Description: A MVC framework for WordPress.
 * Version:     1.0
 * Author:      The Code Co
 * Author URI:  https://thecode.co
 *
 * @package wpmvc
 */

namespace WPMVC;

// Load the library.
require dirname( __FILE__ ) . '/vendor/autoload.php';
