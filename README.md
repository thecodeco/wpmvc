
# WPMVC - WordPress MVC Framework [DEPRECATED]

## MOVED TO GITHUB

This repo will no longer be maintained unless critical to patch a security issue.

All new projects will use the GitHub version moving forward.

Please see the new repository: [GitHub](https://github.com/TheCodeCompany/wpmvc)


## Getting Started

When setting up a new project / site, it is best to use our [TCC Base repo](https://bitbucket.org/thecodeco/base/src/main/).

The base repo has an example WPMVC set up, an example/base theme etc.

## Manual Set Up

The best way to get started using this framework is to clone the [example repo](https://bitbucket.org/thecodeco/wpmvc-example/src/).

This is a good base for building a new plugin, including the directory structure, composer set up etc. It also provides some basic examples of controllers etc.

This process is documented here - [WPMVC Set Up](https://thecodeco.atlassian.net/wiki/spaces/DO/pages/143360214/WPMVC+-+Set+Up).
