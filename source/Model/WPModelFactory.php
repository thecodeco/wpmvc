<?php
/**
 * WPModelFactory
 *
 * @package WPMVC\Model
 */

// phpcs:disable WordPress.Files.FileName

namespace WPMVC\Model;

use WPMVC\Core\ModelFactory;

/**
 * Base factory classes for WordPress-based models
 *
 * @package WPMVC\Model
 */
class WPModelFactory extends ModelFactory {

}