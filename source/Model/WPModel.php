<?php
/**
 * Provides the WPModel class.
 *
 * @package Package
 */

// phpcs:disable WordPress.Files.FileName

namespace WPMVC\Model;

use WPMVC\Core\Model;

/**
 * Represent WordPress models. All models that exist in the WordPress
 * environment should extend this class.
 *
 * @package WPMVC\Model
 */
class WPModel extends Model {
}
